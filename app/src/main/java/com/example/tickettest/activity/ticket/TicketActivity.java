package com.example.tickettest.activity.ticket;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tickettest.R;

import java.util.Random;

public class TicketActivity extends AppCompatActivity implements TicketView, AdapterView.OnItemSelectedListener {

    EditText et_subject, et_description;
    ProgressDialog progressDialog;
    String priority;
    Spinner spinner;
    TicketPresenter presenter;
    Menu actionMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        et_subject = findViewById(R.id.subject);
        et_description = findViewById(R.id.description);

        spinner = (Spinner) findViewById(R.id.priority);
        spinner.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.priority_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        //Progress Dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Aguarde...");

        presenter = new TicketPresenter(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        //Toast.makeText(TicketActivity.this, "Selected: " + "id=" + id +" t=" + parent.getItemAtPosition(pos).toString(), Toast.LENGTH_LONG).show();
        priority = parent.getItemAtPosition(pos).toString();

    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_ticket, menu);
        actionMenu = menu;

        actionMenu.findItem(R.id.cancel).setVisible(true);
        actionMenu.findItem(R.id.save).setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String subject = et_subject.getText().toString().trim();
        String description = et_description.getText().toString().trim();

        switch (item.getItemId()) {
            case R.id.save:
                //Save
                if (subject.isEmpty()) {
                    et_subject.setError("Preencha um título");
                } else if (description.isEmpty()) {
                    et_description.setError("Escolha uma descrição");
                } else {
                     Random rand = new Random();
                     int num = rand.nextInt();

                    presenter.saveTicket(Integer.toString(num), subject, description, priority);
                }
                return true;

            case R.id.cancel:

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle("Confirme !");
                    alertDialog.setMessage("Deseja sair sem salvar?");
                    alertDialog.setNegativeButton("Sim", (dialog, which) -> {
                        dialog.dismiss();
                        finish();
                    });
                    alertDialog.setPositiveButton("Cancelar",
                            (dialog, which) -> dialog.dismiss());

                    alertDialog.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgerss() {
        progressDialog.show();
    }

    @Override
    public void hideProgerss() {
        progressDialog.hide();
        progressDialog.dismiss();
    }

    @Override
    public void onAddSuccess(String message) {
        Toast.makeText(TicketActivity.this, message, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onAddError(String message) {
        Toast.makeText(TicketActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
