package com.example.tickettest.activity.main;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tickettest.R;
import com.example.tickettest.model.Tickets;

import java.text.SimpleDateFormat;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.RecyclerViewAdapter> {

    private Context context;
    private List <Tickets> tickets;
    private TicketClickListener ticketClickListener;
    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public MainAdapter(Context context, List<Tickets> tickets, TicketClickListener ticketClickListener) {
        this.context = context;
        this.tickets = tickets;
        this.ticketClickListener = ticketClickListener;
    }

    @NonNull
        @Override
        public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_ticket, parent, false);

            return new RecyclerViewAdapter(view, ticketClickListener);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewAdapter holder, int position) {
            Tickets ticket = tickets.get(position);

            holder.tv_subject.setText(ticket.getSubject());
            holder.tv_description.setText(ticket.getDescription());
            holder.tv_priority.setText(ticket.getPriority());
            holder.tv_priority.setTypeface(null, Typeface.BOLD);
            holder.tv_priority.setPaintFlags(holder.tv_priority.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
            holder.tv_status.setText(ticket.getStatus());
            holder.tv_date.setText(formato.format(ticket.getDate()));

            if(ticket.getStatus().equals("open")){
                holder.line.setBackgroundColor(Color.parseColor("#ff5051"));
            }

        }

        @Override
        public int getItemCount() {
            return tickets.size();
        }

        class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView tv_subject, tv_description, tv_priority, tv_status, tv_date;
            CardView card_ticket;
            LinearLayout line;
            TicketClickListener ticketClickListener;

            RecyclerViewAdapter(@NonNull View itemView, TicketClickListener ticketClickListener ) {
                super(itemView);

                tv_subject = itemView.findViewById(R.id.subject);
                tv_description = itemView.findViewById(R.id.description);
                tv_priority = itemView.findViewById(R.id.priority);
                tv_status = itemView.findViewById(R.id.status);
                tv_date = itemView.findViewById(R.id.date);

                card_ticket = itemView.findViewById(R.id.card_ticket);
                line = itemView.findViewById(R.id.line);

                this.ticketClickListener = ticketClickListener;
                card_ticket.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                ticketClickListener.onTicketClick(v, getAdapterPosition());
            }
        }

        public interface TicketClickListener{
            void onTicketClick(View view, int position);
        }
}
