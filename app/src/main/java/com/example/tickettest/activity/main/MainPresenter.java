package com.example.tickettest.activity.main;

import com.example.tickettest.api.ApiClient;
import com.example.tickettest.api.ApiInterface;
import com.example.tickettest.model.Ticket;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter {

    private MainView view;

    public MainPresenter(MainView view) {
        this.view = view;
    }

    void getData(){

        view.showLoading();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Ticket> call = apiInterface.getTicket();

        call.enqueue(new Callback<Ticket>() {
            @Override
            public void onResponse(Call<Ticket> call, Response<Ticket> response) {
                view.hideLoading();

                if(response.isSuccessful() || response.body() != null){
                    view.onGetResult(response.body().getTickets());
                }
            }

            @Override
            public void onFailure(Call<Ticket> call, Throwable t) {
                view.hideLoading();
                view.onErrorLoading(t.getLocalizedMessage());
            }
        });
    }
}
