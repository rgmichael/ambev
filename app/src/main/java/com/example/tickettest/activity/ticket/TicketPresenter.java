package com.example.tickettest.activity.ticket;

import androidx.annotation.NonNull;

import com.example.tickettest.api.ApiClient;
import com.example.tickettest.api.ApiInterface;
import com.example.tickettest.model.Ticket;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketPresenter {

    private TicketView ticketView;

    public TicketPresenter(TicketView ticketView) {
        this.ticketView = ticketView;
    }

    void saveTicket(String num,String subject, String description,String priority){
        ticketView.showProgerss();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<Ticket> call = apiInterface.save(num,subject,description,priority);

        call.enqueue(new Callback<Ticket>() {

            @Override
            public void onResponse(@NonNull Call<Ticket> call, @NonNull Response<Ticket> response) {

                ticketView.hideProgerss();

                if(response.isSuccessful() || response.body() != null){

                    if(response.isSuccessful()){
                        ticketView.onAddSuccess(response.message());

                    }else{
                        ticketView.onAddError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Ticket> call, @NonNull Throwable t) {
                ticketView.hideProgerss();
                ticketView.onAddError(t.getLocalizedMessage());

            }
        });

    }

}
