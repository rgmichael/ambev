package com.example.tickettest.activity.main;

import com.example.tickettest.model.Tickets;

import java.util.List;

public interface MainView {

    void showLoading();
    void hideLoading();
    void onGetResult(List<Tickets> tickets);
    void onErrorLoading(String message);
}
