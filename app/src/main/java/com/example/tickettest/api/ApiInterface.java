package com.example.tickettest.api;

import com.example.tickettest.model.Ticket;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("tickets")
    Call<Ticket> save(
            @Field("id") String id,
            @Field("subject") String subject,
            @Field("description") String description,
            @Field("priority") String priority
    );

    @Headers({
            "Content-Type: application/json",
            "Authorization: Basic YW5ncmFlbGxAZ21haWwuY29tOkFuMzUyOTgyOTc="
    })

    @GET("api/v2/tickets.json")
    Call<Ticket> getTicket();

}
