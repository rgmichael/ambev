package com.example.tickettest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Tickets {

    @Expose
    @SerializedName("subject") private String subject;
    @Expose
    @SerializedName("description") private String description;
    @Expose
    @SerializedName("status") private String status;
    @Expose
    @SerializedName("priority") private String priority;
    @Expose
    @SerializedName("created_at") private Date date;


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }
}
